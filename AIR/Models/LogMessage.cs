using System;
using System.Collections.Generic;

namespace AIR.Models
{
    /// <summary>
    /// LogMessage class is used to represent the message sent from client RemoteLogger to the server. It has the same
    /// content as the JSON object which client generates. 
    /// </summary>
    public class LogMessage
    {
        /// <summary>
        /// Unique UUID assigned to the client at each program startup.
        /// </summary>
        public Guid sessionGuid;

        /// <summary>
        /// List of tags used to build a hierarchical structure of the log.
        /// </summary>
        public List<string> tags;

        // TODO: think about this. This could introduce a very sparse hierarchy and render logs unreadable, since 
        /// <summary>
        /// List of strings used as a values for tags provided in <see cref="tags"/> list. 
        /// </summary>
        // public List<string> tagValues;

        /// <summary>
        /// The log message itself.
        /// </summary>
        public string message;

        public override string ToString()
        {
            return $"{{sessionGuid: {sessionGuid.ToString()}, tags: [{string.Join(" -> ", tags)}], message: {message}}}";
        }
    }
}
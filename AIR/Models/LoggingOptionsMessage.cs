using System.Collections.Generic;

namespace AIR.Models
{
    public class LoggingOptionsMessage
    {
        /// <summary>
        /// A very general option container that allows to set arbitrary options with dictionary.
        /// </summary>
        private Dictionary<string, string> options;
    }
}
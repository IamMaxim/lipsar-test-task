using System;
using System.Collections.Generic;

namespace AIR.Models
{
    public class MetadataMessage
    {
        public Guid sessionGuid;
        /// <summary>
        /// Dictionary containing client's metadata. Can have arbitrary data.
        /// </summary>
        public Dictionary<string, string> metadata;

        public override string ToString()
        {
            return $"{{sessionGuid: {sessionGuid.ToString()}, metadata: {metadata}}}";
        }
    }
}
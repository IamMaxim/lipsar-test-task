using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using AIR.OptionsProviders;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Misc.Schemes;
using Newtonsoft.Json;

namespace AIR
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Allow MVC controllers to set Content-Type to application/xml.
            // I didn't find more beautiful solution how to do this (for example, in controller itself).
            services.AddMvc(options =>
                options.OutputFormatters.OfType<StringOutputFormatter>().Single().SupportedMediaTypes
                    .Add("application/xml"));

            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Full;
                    options.SerializerSettings.TypeNameHandling = TypeNameHandling.Auto;
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo()
                {
                    Title = "AIR API",
                    Version = "v0.1a"
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "forbidScheme";
                options.DefaultForbidScheme = "forbidScheme";
                options.AddScheme<ForbidScheme>("forbidScheme", "Handle Forbidden");
            });

            services.AddSingleton<IOptionProviderManager>(provider =>
            {
                var manager = new OptionProviderManager();

                // Add severity option provider with all severity levels listed
                manager.AddProvider(new SeverityOptionProvider(new List<SeverityOptionProvider.Severity>
                {
                    SeverityOptionProvider.Severity.Info,
                    SeverityOptionProvider.Severity.Warning,
                    SeverityOptionProvider.Severity.Error,
                }));

                return manager;
            });
            services.AddSingleton<ITaggedLogger>(new TaggedLogger());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();

                app.UseForwardedHeaders(new ForwardedHeadersOptions
                {
                    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
                });
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            // app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1"); });
        }
    }
}
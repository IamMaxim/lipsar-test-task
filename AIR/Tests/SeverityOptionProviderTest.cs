using System;
using System.Collections.Generic;
using System.Linq;
using AIR.OptionsProviders;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;
using static AIR.OptionsProviders.SeverityOptionProvider.Severity;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace AIR
{
    public class SeverityOptionProviderTest
    {
        private readonly ITestOutputHelper m_TestOutputHelper;

        public SeverityOptionProviderTest(ITestOutputHelper testOutputHelper)
        {
            m_TestOutputHelper = testOutputHelper;
        }

        [Fact]
        public void TestSeverityOptionProvider()
        {
            var provider = new SeverityOptionProvider(new List<SeverityOptionProvider.Severity>
            {
                Info,
                Warning
            });

            // Check that produced JSON is correct
            Assert.Equal(
                "{\"severities\":[\"Info\",\"Warning\"]}",
                JsonConvert.SerializeObject(
                    provider.ProvideOptions(),
                    new Newtonsoft.Json.Converters.StringEnumConverter()
                ));
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace AIR
{
    public class SafeDictionaryTest
    {
        
        /// <summary>
        /// Tests that safe dictionary actually works (automatically creates a new value for a requested
        /// key if it doesn't exist).
        /// </summary>
        [Fact]
        public void TestSafeDictionary()
        {
            var dict = new SafeDictionary<string, List<object>>(key => new List<object>());
             
            // Check that new empty object is created for non-existing key-value pair
            var l = dict["first"];
            var empty = new List<object>();
            Assert.True(Enumerable.SequenceEqual(l, empty));
            // // Check that object was stored in the dictionary by comparing pointers
            Assert.Same(l, dict["first"]);
        }
    }
}
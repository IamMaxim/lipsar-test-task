using System;
using System.Collections.Generic;

namespace AIR
{
    public class SafeDictionary<TKey, TValue> : Dictionary<TKey, TValue>
    {
        public delegate TValue ValueConstructor(TKey key);

        private ValueConstructor _defaultConstructor;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultConstructor">The value constructor that is called when no value associated with
        /// given key exists.</param>
        public SafeDictionary(ValueConstructor defaultConstructor)
        {
            _defaultConstructor = defaultConstructor;
        }

        /// <summary>
        /// Safely returns a value from the dictionary. If value associated with the given key does not exists,
        /// creates a new value, associates it with the given key and returns that new value.
        /// </summary>
        /// <param name="key"></param>
        public TValue this[TKey key]
        {
            get
            {
                // Simple but proven way to avoid concurrency problems.
                lock (this)
                {
                    TValue s;
                    try
                    {
                        // Try to retrieve value from the dictionary
                        s = base[key];
                    }
                    catch (KeyNotFoundException e)
                    {
                        // No value is stored; create new one and put it into the dictionary
                        s = _defaultConstructor(key);
                        Add(key, s);
                    }

                    return s;
                }
            }
        }
    }
}
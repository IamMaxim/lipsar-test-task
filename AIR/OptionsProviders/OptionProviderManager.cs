using System.Collections.Generic;

namespace AIR.OptionsProviders
{
    /// <summary>
    /// Default implementation of the option provider manager. Simply adds all the options from all the registered
    /// providers.
    /// </summary>
    public class OptionProviderManager : IOptionProviderManager
    {
        private List<IOptionProvider> providers = new List<IOptionProvider>();

        /// <summary>
        /// Registers a provider in the option provider manager.
        /// </summary>
        /// <param name="provider">Provider to register.</param>
        public void AddProvider(IOptionProvider provider)
        {
            providers.Add(provider);
        }

        /// <summary>
        /// Builds an options dictionary from all providers registered in the options provider manager.
        /// </summary>
        /// <returns>Dictionary with all the options.</returns>
        public Dictionary<string, object> BuildOptions()
        {
            var result = new Dictionary<string, object>();

            providers.ForEach(p =>
            {
                foreach (var (key, value) in p.ProvideOptions())
                    result[key] = value;
            });

            return result;
        }
    }
}
using System.Collections.Generic;

namespace AIR.OptionsProviders
{
    /// <summary>
    /// Option provider that allows to set different log severity levels to send.
    /// </summary>
    public class SeverityOptionProvider : IOptionProvider
    {
        public enum Severity
        {
            Info,
            Warning,
            Error
        }

        /// <summary>
        /// Contains all the requested severity levels that are set to the client.
        /// </summary>
        public List<Severity> Severities { get; }

        public SeverityOptionProvider(List<Severity> severities)
        {
            Severities = severities;
        }

        public Dictionary<string, object> ProvideOptions()
        {
            return new Dictionary<string, object>()
            {
                {"severities", Severities}
            };
        }
    }
}
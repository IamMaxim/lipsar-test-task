using System.Collections.Generic;

namespace AIR.OptionsProviders
{
    /// <summary>
    /// Provides a Dependency Injection way to setup option providers. 
    /// </summary>
    public interface IOptionProviderManager
    {
        /// <summary>
        /// Registers a provider to be used when client requests options.
        /// </summary>
        /// <param name="provider">The provider to register.</param>
        public void AddProvider(IOptionProvider provider);

        /// <summary>
        /// Assembles options from all providers into one dictionary suitable for sending.
        /// </summary>
        public Dictionary<string, object> BuildOptions();
    }
}
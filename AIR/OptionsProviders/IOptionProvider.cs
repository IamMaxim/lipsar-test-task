using System.Collections.Generic;

namespace AIR.OptionsProviders
{
    public interface IOptionProvider
    {
        /// <summary>
        /// Returns all the options set by the current option provider. Options from all providers are combined into
        /// one dictionary before sending to the client.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> ProvideOptions();
    }
}
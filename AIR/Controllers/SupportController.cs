﻿using System;
using Microsoft.AspNetCore.Mvc;
using Misc.Attributes;


namespace AIR.Controllers
{
    [ApiController]
    [ErrorSafe] // todo review
    //[AuthToken] // todo review
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]
    public class SupportController : Controller
    {
        /// <summary>
        /// Pongs back what you send, you know.
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        [Route("ping")]
        [HttpGet]
        public IActionResult Ping([FromQuery] string arg)
        {
            Console.WriteLine(arg);
            return Ok(arg);
        }
    }
}
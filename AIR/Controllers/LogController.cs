using System;
using AIR.Models;
using AIR.OptionsProviders;
using Microsoft.AspNetCore.Mvc;
using Misc.Attributes;
using Newtonsoft.Json;

namespace AIR.Controllers
{
    [ApiController]
    [ErrorSafe] // todo review
    //[AuthToken] // todo review
    [ProducesResponseType(200)]
    [ProducesResponseType(405)]
    public class LogController : Controller
    {
        /// <summary>Cached logger service instance.</summary>
        private ITaggedLogger logger;

        /// <summary>Cached option provider manager service instance.</summary>
        private IOptionProviderManager optionProviderManager;

        public LogController(ITaggedLogger logger, IOptionProviderManager optionProviderManager)
        {
            this.logger = logger;
            this.optionProviderManager = optionProviderManager;
        }

        /// <summary>
        /// Adds a log message to the server's internal log storage.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     {
        ///         "sessionGuid": "C871237E-B6E7-4F28-8706-860917A87E7B",
        ///         "tags": ["AIR.Controllers.LogController", "Log"],
        ///         "message": "A message produced!"
        ///     }
        /// 
        /// </remarks>
        /// <param name="message" example="object">An JSON object representing a log message.</param>
        /// <response code="200">Log message was added successfully.</response>
        /// <response code="405">Probably, you're trying to make GET request instead of POST request.</response>
        [Route("log")]
        [HttpPost]
        public IActionResult Log([FromBody] LogMessage message)
        {
            logger.Log(message.sessionGuid, message.tags, message.message);
            return Ok();
        }

        /// <summary>
        /// Sets the particular session's metadata to the passed one.
        /// </summary>
        /// <param name="message">An JSON object representing a metadata message, containing
        /// session GUID and dictionary.</param>
        /// <remarks>
        /// Sample request:
        /// 
        ///     {
        ///         "sessionGuid": "C871237E-B6E7-4F28-8706-860917A87E7B",
        ///         "tags": ["AIR.Controllers.LogController", "Log"],
        ///         "message": "A message produced!"
        ///     }
        /// 
        /// </remarks>
        /// <response code="200">Metadata was set successfully.</response>
        /// <response code="405">Probably, you're trying to make GET request instead of POST request.</response>
        [Route("set_metadata")]
        [HttpPost]
        public IActionResult SetMetadata([FromBody] MetadataMessage message)
        {
            logger.SetMetadata(message.sessionGuid, message.metadata);
            return Ok();
        }

        /// <summary>
        /// Returns an XML containing all the logs stored for the current service run.
        /// </summary>
        /// <returns>XML with all logs.</returns>
        [Route("log")]
        [HttpGet]
        [Produces("application/xml")]
        public ActionResult<string> GetLog()
        {
            return logger.SerializeXml();
        }

        /// <summary>
        /// Builds a list of options for the client.
        /// </summary>
        /// <returns>JSON object representing list of options.</returns>
        [Route("options")]
        [HttpGet]
        [Produces("text/plain")]
        public ActionResult<string> GetOptions()
        {
            return JsonConvert.SerializeObject(
                optionProviderManager.BuildOptions(),
                new Newtonsoft.Json.Converters.StringEnumConverter()
            );
        }
    }
}
using System;
using System.Collections.Generic;

namespace AIR
{
    /// <summary>
    /// Logger with support for hierarchical representation.
    /// </summary>
    public abstract class ITaggedLogger
    {
        public abstract void Log(Guid sessionId, List<String> tags, String message);

        public abstract void SetMetadata(Guid sessionId, Dictionary<string, string> metadata);
        
        public abstract string SerializeXml();
    }
}
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using AIR;

namespace TaggedLogger
{
    public class TaggedLoggerState
    {
        private SafeDictionary<Guid, Session> sessions = new SafeDictionary<Guid, Session>(guid => new Session(guid));
        private DateTime start = DateTime.Now;
        private DateTime end = DateTime.Now;

        // Handy substitution to allow writing `state[guid]...` instead of `state.sessions[guid]...`
        public Session this[Guid sessionGuid] => sessions[sessionGuid];

        /// <summary>
        /// Serializes TaggedLoggerState into the indented XML document (including all the required parts).
        /// </summary>
        /// <returns>String containing XML representation of the TaggedLoggerState.</returns>
        public string SerializeXml()
        {
            // Use settings to enable indentation of XML (for ease of reading).
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            var sw = new StringWriter();
            var writer = XmlWriter.Create(sw, settings);

            writer.WriteStartDocument();
            writer.WriteStartElement("LoggerState");
            writer.WriteAttributeString("start", start.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("end", end.ToString(CultureInfo.InvariantCulture));

            foreach (var (_, value) in sessions)
            {
                value.SerializeXml(writer);
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();

            writer.Flush();
            return sw.ToString();
        }
    }

    public class Session : SafeDictionary<Guid, LogEntry>
    {
        private Guid sessionGuid;
        private DateTime start = DateTime.Now;
        private DateTime end = DateTime.Now;
        private LogEntry root = new LogEntry("root");
        public Dictionary<string, string> metadata = new Dictionary<string, string>();

        public Session(Guid sessionGuid) : base(tag => new LogEntry("should never be created"))
        {
            this.sessionGuid = sessionGuid;
        }

        public LogEntry this[string tag] => root[tag];
        public void AddLog(string msg) => root.AddLog(msg);

        /// <summary>
        /// Writes Session to the passed XmlWriter. This is not an independent document, just a part of it.
        /// </summary>
        public void SerializeXml(XmlWriter writer)
        {
            writer.WriteStartElement("Session");
            // Write session's ID and start/end time
            writer.WriteAttributeString("guid", sessionGuid.ToString());
            writer.WriteAttributeString("start", start.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("end", end.ToString(CultureInfo.InvariantCulture));

            // Write all the metadata
            writer.WriteStartElement("metadata");
            foreach (var (key, value) in metadata)
                writer.WriteAttributeString(key, value);
            writer.WriteEndElement();

            root.SerializeXml(writer);
            writer.WriteEndElement();
        }
    }

    public class LogEntry : SafeDictionary<string, LogEntry>
    {
        private List<string> log = new List<string>();
        private string tag;

        public LogEntry(string tag) : base(tag => new LogEntry(tag))
        {
            this.tag = tag;
        }

        /// <summary>
        /// Adds a message to the current LogEntry.
        /// </summary>
        /// <param name="msg">Message to add.</param>
        public void AddLog(string msg) => log.Add(msg);

        /// <summary>
        /// Returns a list of all logs stored in the current LogEntry.
        /// </summary>
        public List<string> GetLogs()
        {
            return log;
        }

        /// <summary>
        /// Writes LogEntry to the passed XmlWriter. This is not an independent document, just a part of it.
        /// </summary>
        public void SerializeXml(XmlWriter writer)
        {
            writer.WriteStartElement("LogEntry");
            writer.WriteAttributeString("name", tag);

            foreach (var (_, value) in this)
                value.SerializeXml(writer);

            foreach (var s in log)
                writer.WriteElementString("Log", s);

            writer.WriteEndElement();
        }
    }
}
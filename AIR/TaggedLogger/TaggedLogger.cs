using System;
using System.Collections.Generic;
using System.Linq;
using RemoteLogger;
using TaggedLogger;

namespace AIR
{
    /// <summary>
    /// This class is intended to be used as a singleton and added with .AddSingleton() in ConfigureServices().
    /// </summary>
    public class TaggedLogger : ITaggedLogger
    {
        private TaggedLoggerState state = new TaggedLoggerState();

        public override void Log(Guid sessionId, List<string> tags, string message)
        {
            // Get the session object for the given GUID
            var session = state[sessionId];

            if (!tags.Any())
                // If no tags provided, put the message in the root
                session.AddLog(message);
            else
            {
                // Find the needed log entry by tags
                var logEntry = session[tags[0]];
                for (var i = 1; i < tags.Count; i++)
                    logEntry = logEntry[tags[i]];

                // Put the message in the newly acquired LogEntry 
                logEntry.AddLog(message);
            }
        }

        public override void SetMetadata(Guid sessionId, Dictionary<string, string> metadata)
        {
            // Get the session object for the given GUID
            var session = state[sessionId];

            // Add all the values of the passed metadata to the session's stored metadata. Override values if needed.
            foreach (var (key, value) in metadata)
                session.metadata.Add(key, value);
        }

        public override string SerializeXml()
        {
            return state.SerializeXml();
        }
    }
}
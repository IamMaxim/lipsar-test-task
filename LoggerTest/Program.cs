﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;
using SimpleJSON;

namespace RemoteLogger
{
    class Program
    {
        static async Task VerboseLog(string message) => await VerboseLog(new List<string>(), message);

        static async Task VerboseLog(List<string> tags, string message)
        {
            var response = await RemoteLogger.Log(tags, message);

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                Console.WriteLine("Log is successfully sent.");
                Console.WriteLine($"Code: {((int) response.StatusCode).ToString()}; Response: {response.Content}");
            }
            else
            {
                Console.WriteLine("ERROR occurred while sending logs:");
                Console.WriteLine(response.ErrorException);
            }
        }

        static async Task VerboseSendMetadata(Dictionary<string, string> metadata)
        {
            var response = await RemoteLogger.SendMetadata(metadata);

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                Console.WriteLine("Metadata is successfully sent.");
                Console.WriteLine($"Code: {((int) response.StatusCode).ToString()}; Response: {response.Content}");
            }
            else
            {
                Console.WriteLine("ERROR occurred while sending metadata:");
                Console.WriteLine(response.ErrorException);
            }
        }
        
        static async Task<JSONObject> VerboseGetOptions()
        {
            var response = await RemoteLogger.GetOptions();

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                Console.WriteLine("Options are successfully received.");
                Console.WriteLine($"Code: {((int) response.StatusCode).ToString()}; Response: {response.Content}");
                return (JSONObject) JSONNode.Parse(response.Content);
            }
            else
            {
                Console.WriteLine("ERROR occurred while sending metadata:");
                Console.WriteLine(response.ErrorException);
                return null;
            }
        }

        static async Task Main(string[] args)
        {
            IMetadataProvider metadataProvider = new OSMetadataProvider();
            
            await VerboseSendMetadata(metadataProvider.GetMetadata());

            var options = await VerboseGetOptions();
            Console.WriteLine("=== Options ===");
            Console.WriteLine(options.ToString(2));
            Console.WriteLine("===============");
            
            await VerboseLog("test me plz");
            await VerboseLog("test me plz 2");
            await VerboseLog("test me plz 3");
            await VerboseLog(new List<string> {"tag1"}, "test me plz 4");
            await VerboseLog(new List<string> {"tag1", "tag2"}, "test me plz 5");
            await VerboseLog(new List<string> {"tag1", "tag2"}, "test me plz 6");
            await VerboseLog(new List<string> {"tag1", "tag3"}, "test me plz 7");
        }
    }
}
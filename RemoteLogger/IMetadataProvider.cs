using System.Collections.Generic;

namespace RemoteLogger
{
    public interface IMetadataProvider
    {
        /// <summary>
        /// Returns all the metadata that should be sent to the server on startup.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetMetadata();
    }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;
using SimpleJSON;

namespace RemoteLogger
{
    // this is client side implementation; Unity doesn't like third party dependencies;   
    public static class RemoteLogger
    {
        // not good, but for test task ok; In reality it will be configured in Unity
        private const string Url = "http://localhost:51228";

        // not good, but for test task ok; In reality it will be configured in Unity
        private const string Token = "QHzNZvsFzqvhy/VcURXzLqrRliFwRJQ+puZQ0a9xBWE=";

        // not sure if it is good, but I assume it is fine to assign one unique session to each program run
        private static readonly Guid SessionGuid = Guid.NewGuid();


        public static async void Log(Exception e)
        {
            Log(e.ToString());
        }

        public static async void Log(List<string> tags, Exception e)
        {
            Log(tags, e.ToString());
        }

        public static async Task<IRestResponse> Log(string msg)
        {
            return await Log(new List<string>(), msg);
        }

        public static async Task<IRestResponse> Log(List<string> tags, string msg)
        {
            return await Log(SessionGuid, tags, msg);
        }

        public static async Task<IRestResponse> SendMetadata(Dictionary<string, string> metadata)
        {
            return await SendMetadata(SessionGuid, metadata);
        }

        public static async Task<IRestResponse> SendMetadata(Guid sessionGuid, Dictionary<string, string> metadata)
        {
            try
            {
                var reqJson = new JSONObject();
                reqJson["sessionGuid"] = sessionGuid.ToString();
                var metadataJson = reqJson["metadata"] = new JSONObject();
                foreach (var pair in metadata)
                    metadataJson[pair.Key] = pair.Value;

                var str = $"{reqJson}";
                Console.WriteLine($"Gonna send {str}");

                return await SendPostJson($"{Url}/set_metadata", reqJson.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public static async Task<IRestResponse> Log(Guid sessionGuid, List<string> tags, string msg)
        {
            try
            {
                var reqJson = new JSONObject();
                reqJson["sessionGuid"] = sessionGuid.ToString();
                reqJson["tags"] = new JSONArray();
                tags.ForEach(t => reqJson["tags"].Add(t));
                reqJson["message"] = msg;
                
                var str = $"{reqJson}";
                Console.WriteLine($"Gonna send {str}");

                return await SendPostJson($"{Url}/log", reqJson.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public static void ApplyOptions(JSONObject options)
        {
            
        }

        public static async Task<IRestResponse> GetOptions()
        {
            try
            {
                return await SendGet($"{Url}/options");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        private static async Task<IRestResponse> SendPostJson(string url, string jsonString)
        {
            // Remember not to use newton.json, only SimpleJSON.
            // Trust me it won't work on some platforms, for example on HoloLens

            // request.AddHeader("Authorization", $"Bearer {Token}");

            var client = new RestClient(url) {Timeout = -1};
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json-patch+json");
            request.AddHeader("accept", "*/*");
            request.AddJsonBody(jsonString);

            return await client.ExecuteAsync(request);
        }

        public static async Task<IRestResponse> SendGet(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            return await client.ExecuteAsync(request);
        }
    }
}
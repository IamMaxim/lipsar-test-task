using System.Collections.Generic;

namespace RemoteLogger
{
    public class OSMetadataProvider : IMetadataProvider
    {
        public Dictionary<string, string> GetMetadata()
        {
            return new Dictionary<string, string>
            {
                {"os", System.Runtime.InteropServices.RuntimeInformation.OSDescription}
            };
        }
    }
}